# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# lilo.panic, 2016
# Sergey Glita <gsv70@mail.ru>, 2012
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:43-0400\n"
"PO-Revision-Date: 2018-09-12 07:47+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Russian (http://www.transifex.com/rosarior/mayan-edms/language/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: apps.py:48 permissions.py:8 settings.py:10
msgid "Document signatures"
msgstr "Подписи документа"

#: apps.py:88
msgid "Date"
msgstr "Дата"

#: apps.py:91 models.py:46
msgid "Key ID"
msgstr "ID ключа"

#: apps.py:95 forms.py:64 models.py:50
msgid "Signature ID"
msgstr "ID подписи"

#: apps.py:96 forms.py:76
msgid "None"
msgstr "Ни один"

#: apps.py:99
msgid "Type"
msgstr "Тип"

#: forms.py:21
msgid "Key"
msgstr "Ключ"

#: forms.py:25
msgid "Passphrase"
msgstr "Кодовая фраза"

#: forms.py:46
msgid "Signature is embedded?"
msgstr "Подпись встроена?"

#: forms.py:48
msgid "Signature date"
msgstr "Дата подписи"

#: forms.py:51
msgid "Signature key ID"
msgstr "ID ключа подписи"

#: forms.py:53
msgid "Signature key present?"
msgstr "Ключ подписи предоставлен?"

#: forms.py:66
msgid "Key fingerprint"
msgstr "Отпечаток ключа"

#: forms.py:70
msgid "Key creation date"
msgstr "Дата создания ключа"

#: forms.py:75
msgid "Key expiration date"
msgstr "Дата устаревания ключа"

#: forms.py:80
msgid "Key length"
msgstr "Длина ключа"

#: forms.py:84
msgid "Key algorithm"
msgstr "Алгоритм ключа"

#: forms.py:88
msgid "Key user ID"
msgstr "ID пользователя ключа"

#: forms.py:92
msgid "Key type"
msgstr "Тип ключа"

#: links.py:36
msgid "Verify all documents"
msgstr "Проверить все документы"

#: links.py:43 links.py:61 queues.py:8
msgid "Signatures"
msgstr "Подписи"

#: links.py:49
msgid "Delete"
msgstr "Удалить"

#: links.py:54
msgid "Details"
msgstr "Детали"

#: links.py:67
msgid "Download"
msgstr "Скачать"

#: links.py:73
msgid "Upload signature"
msgstr "Вгрузить подпись"

#: links.py:79
msgid "Sign detached"
msgstr "Подпись отеделена"

#: links.py:85
msgid "Sign embedded"
msgstr "Подпись встроена"

#: models.py:40
msgid "Document version"
msgstr "Версия документа"

#: models.py:44
msgid "Date signed"
msgstr "Дата подписи"

#: models.py:54
msgid "Public key fingerprint"
msgstr "Отпечаток публичного ключа"

#: models.py:60
msgid "Document version signature"
msgstr "Подпись версии документа"

#: models.py:61
msgid "Document version signatures"
msgstr "Подписи версий документов"

#: models.py:80
msgid "Detached"
msgstr "Отделена"

#: models.py:82
msgid "Embedded"
msgstr "Встроена"

#: models.py:97
msgid "Document version embedded signature"
msgstr "Встроенная подпись версии документа"

#: models.py:98
msgid "Document version embedded signatures"
msgstr "Встроенные подписи версий документов"

#: models.py:131
msgid "Signature file"
msgstr "Файл подписи"

#: models.py:138
msgid "Document version detached signature"
msgstr "Отделённая подпись версии документа"

#: models.py:139
msgid "Document version detached signatures"
msgstr "Отделённые подписи версий документов"

#: models.py:142
msgid "signature"
msgstr "подпись"

#: permissions.py:13
msgid "Sign documents with detached signatures"
msgstr "Подписать документы отделёнными подписями"

#: permissions.py:17
msgid "Sign documents with embedded signatures"
msgstr "Подписать документы встроенными подписями"

#: permissions.py:21
msgid "Delete detached signatures"
msgstr "Удаление отделенных подписей"

#: permissions.py:25
msgid "Download detached document signatures"
msgstr "Скачать отделенные подписи документов"

#: permissions.py:29
msgid "Upload detached document signatures"
msgstr "Вгрузить отделенные подписи документов"

#: permissions.py:33
msgid "Verify document signatures"
msgstr "Проверить подпись документа"

#: permissions.py:37
msgid "View details of document signatures"
msgstr "Посмотреть подробности подписей документов"

#: queues.py:11
msgid "Verify key signatures"
msgstr ""

#: queues.py:15
msgid "Unverify key signatures"
msgstr ""

#: queues.py:19
msgid "Verify document version"
msgstr ""

#: queues.py:24
msgid "Verify missing embedded signature"
msgstr ""

#: settings.py:14
msgid "Path to the Storage subclass to use when storing detached signatures."
msgstr ""

#: settings.py:23
msgid "Arguments to pass to the SIGNATURE_STORAGE_BACKEND. "
msgstr ""

#: views.py:68 views.py:161
msgid "Passphrase is needed to unlock this key."
msgstr "Для разблокироваки этого ключа необходима кодовая фраза"

#: views.py:78 views.py:171
msgid "Passphrase is incorrect."
msgstr "Кодовая фраза неверна."

#: views.py:99 views.py:191
msgid "Document version signed successfully."
msgstr "Версия документа успешно подписана."

#: views.py:123
#, python-format
msgid "Sign document version \"%s\" with a detached signature"
msgstr "Подписать версию документа \"%s\" отделённой подписью"

#: views.py:222
#, python-format
msgid "Sign document version \"%s\" with a embedded signature"
msgstr "Подписать версию документа \"%s\" встроенной подписью"

#: views.py:245
#, python-format
msgid "Delete detached signature: %s"
msgstr "Удалить отделённую подпись: %s"

#: views.py:266
#, python-format
msgid "Details for signature: %s"
msgstr "Подробности для подписи: %s"

#: views.py:306
msgid ""
"Signatures help provide authorship evidence and tamper detection. They are "
"very secure and hard to forge. A signature can be embedded as part of the "
"document itself or uploaded as a separate file."
msgstr ""

#: views.py:328
msgid "There are no signatures for this document."
msgstr ""

#: views.py:331
#, python-format
msgid "Signatures for document version: %s"
msgstr "Подписи для документа версии: %s"

#: views.py:360
#, python-format
msgid "Upload detached signature for document version: %s"
msgstr "Выгрузить отделённую подпись для версии документа: %s"

#: views.py:377
msgid "On large databases this operation may take some time to execute."
msgstr "В больших базах данных эта операция может занять некоторое время для выполнения."

#: views.py:378
msgid "Verify all document for signatures?"
msgstr "Проверить подписи во всех документах?"

#: views.py:388
msgid "Signature verification queued successfully."
msgstr "Верификация сигнатуры добавлена в очередь."
